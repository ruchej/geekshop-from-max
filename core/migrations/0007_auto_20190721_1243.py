# Generated by Django 2.2.2 on 2019-07-21 10:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0006_auto_20190714_1046'),
    ]

    operations = [
        migrations.AddField(
            model_name='menu',
            name='css_class',
            field=models.CharField(default='', max_length=30, verbose_name='CSS-Класс элемента в меню'),
        ),
        migrations.AddField(
            model_name='menu',
            name='seen_gasts',
            field=models.BooleanField(default=True, verbose_name='Элемент меню виден незарегистрированным пользователям'),
        ),
        migrations.AddField(
            model_name='menu',
            name='seen_users',
            field=models.BooleanField(default=False, verbose_name='Элемент меню виден зарегистрированным пользователям'),
        ),
    ]
