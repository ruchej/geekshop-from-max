# Generated by Django 2.2.2 on 2019-07-13 14:25

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Address',
            fields=[
                ('core_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='core.Core')),
                ('address1', models.CharField(blank=True, max_length=256, null=True, verbose_name='Адрес-street')),
                ('adвress2', models.CharField(blank=True, max_length=256, null=True, verbose_name='Адрес-number')),
                ('zip_code', models.CharField(blank=True, max_length=20, null=True, verbose_name='Адрес-zip')),
                ('city', models.CharField(blank=True, max_length=256, null=True, verbose_name='Адрес-city')),
                ('region', models.CharField(blank=True, max_length=256, null=True, verbose_name='Адрес-region')),
                ('country', models.CharField(blank=True, max_length=256, null=True, verbose_name='Адрес-country')),
                ('phone', models.CharField(blank=True, max_length=256, null=True, verbose_name='Адрес-phone')),
                ('contact_person', models.CharField(blank=True, max_length=256, null=True, verbose_name='Адрес-person')),
                ('related_obj', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='addresses', to='core.Core', verbose_name='addresses')),
            ],
            options={
                'verbose_name': 'Адрес',
                'verbose_name_plural': 'Адреса',
                'ordering': ('sort', 'title'),
            },
            bases=('core.core',),
        ),
    ]
