# -*- coding: utf-8 -*-

from django.conf import settings
from .models import Menu

MENU = Menu.objects.all()
# from django.utils.translation import ugettext_lazy as _


def get_version(request):
    return {'VERSION': settings.VERSION}


def get_menus(request):
    return {'menu': MENU}
