from django.contrib import admin
from django.contrib.sessions.models import Session
from django.contrib.contenttypes.models import ContentType

from django.contrib.admin.models import LogEntry, ADDITION, CHANGE, DELETION

from .models import Address, Picture, Menu, Core
from django.contrib import messages
# Register your models here.
admin.site.register(Address)
admin.site.register(Picture)

admin.site.disable_action('delete_selected')

def soft_delete(self, request, queryset):
    counter = 0
    for obj in queryset:
        obj.active = False
        obj.save()
        counter += 1

        LogEntry.objects.log_action(user_id=request.user.id,
                                    content_type_id=ContentType.objects.get(app_label=type(obj)._meta.app_label, model=type(obj).__name__.lower()).id,
                                    object_id=obj.id,
                                    object_repr=obj.__repr__(),
                                    action_flag=DELETION,
                                    change_message='архивация объекта')

    messages.info(request, f'{counter} объектов заархивировано')


soft_delete.short_description = 'удаление'


admin.site.add_action(soft_delete)


class MenuAdmin(admin.ModelAdmin):
    """docstring for ProductAdmin"""
    list_display = ('__str__', 'css_class', 'seen_gasts', 'seen_users', 'parent')


admin.site.register(Menu, MenuAdmin)


class SessionAdmin(admin.ModelAdmin):

    def has_add_permission(self, request):
        return False


admin.site.register(Session, SessionAdmin)


admin.site.register(LogEntry)


def activate(self, request, queryset):
    counter = 0
    for obj in queryset:
        obj.active = True
        obj.save()

        LogEntry.objects.log_action(user_id=request.user.id,
                                    content_type_id=ContentType.objects.get(app_label=type(obj)._meta.app_label, model=type(obj).__name__.lower()).id,
                                    object_id=obj.id,
                                    object_repr=obj.__repr__(),
                                    action_flag=ADDITION,
                                    change_message='восстановление объекта')

        counter += 1
    messages.info(request, f'{counter} объектов восстановлено')


activate.short_description = 'восстановление объектов'


class DeletedProductsAdmin(admin.ModelAdmin):
    """docstring for ProductAdmin"""
    list_display = ('title', 'active')
    actions = (activate,)

    def get_queryset(self, request):
        return super().get_queryset(request).filter(active=False)


admin.site.register(Core, DeletedProductsAdmin)
