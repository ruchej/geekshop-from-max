from django.contrib.auth.views import LoginView, LogoutView, INTERNAL_RESET_URL_TOKEN
from django.views.generic import CreateView, DeleteView, DetailView, UpdateView
from django.utils.translation import gettext_lazy as _
from .models import Account #, # Avatar
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin, PermissionRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth.forms import PasswordResetForm, UserCreationForm
from django.forms import modelform_factory
from django.contrib.auth.views import PasswordResetConfirmView, PasswordResetCompleteView, PasswordResetView
# from django.contrib.auth.forms import UserCreationForm
from .forms import UserRegisterForm, UserActivationRegisterForm #, UserRegisterForm

class UserNotAuthMixin(UserPassesTestMixin):
    url_redirect = '/'
    warning_message = _('Эта опция доступна только для новых посетителей.')

    def test_func(self):
        return self.request.user.is_anonymous  # is_anonymous

    def handle_no_permission(self):
        messages.warning(self.request, self.warning_message)
        return HttpResponseRedirect(self.url_redirect)


class UserAuthMixin(UserNotAuthMixin):
    warning_message = _('Для перехода по этой ссылке пожалуйста залогиньтесь.')

    def test_func(self):
        return self.request.user.is_authenticated  # is_authenticated


class Login(SuccessMessageMixin, UserNotAuthMixin, LoginView):
    success_message = _('Вход в систему выполнен')
    extra_context = {'page_title': _('Login'), 'header_class': 'hero', 'content_class': 'tab-content'}


class Logout(UserAuthMixin, LogoutView):
    success_message = _('Вы вышли из системы')

    def get_next_page(self):
        messages.success(self.request, self.success_message)
        return super().get_next_page()


class AccountCreate(UserNotAuthMixin, SuccessMessageMixin, CreateView, PasswordResetView,):
    model = Account
    extra_context = {'page_title': _('Форма регистрации'), 'header_class': 'hero', }
    form_class = UserRegisterForm
    """
    Registration User with send email and post activation (SignUpConfirmView)
    """
    # '#form_class = modelform_factory(model,
    #                                    form=type('UserRegisterForm', (UserCreationForm, PasswordResetForm), {}),
    #                                    field's=('username', 'first_name', 'last_name', 'email', 'description', 'password1', 'password2'),)
    success_url = reverse_lazy('registration:Login')
    url_redirect = reverse_lazy('registration:AccountsDetail')
    template_name = 'registration/user_signup_form.html'
    email_template_name = 'registration/signup_email.html'
    #subject_template_name = 'registration/signup_subject.txt'
    success_message = _('Для активации аккаунта выслано письмо')


class AccountConfirm(PasswordResetConfirmView):
    """
    Activation registration
    """
    extra_context = {'page_title': _('Подтверждение регистрации'), 'header_class': 'hero', }

    success_url = reverse_lazy('registration:Login')
    template_name = 'registration/signup_confirm.html'
    form_class = UserActivationRegisterForm
    post_reset_login = True
    post_reset_login_backend = 'django.contrib.auth.backends.ModelBackend'
    INTERNAL_RESET_URL_TOKEN = 'set-active'


class AccountDetail(LoginRequiredMixin, DetailView):
    """
    Профиль пользователя
    """

    model = Account
    extra_context = {'page_title': _('Профиль пользователя'), 'header_class': 'hero', }

    def get_object(self, *args, **kwargs):
        return self.request.user


# class changeProfile(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
#     """
#     Изменени пользователя


#     Extends:
#         LoginRequiredMixin
#         UpdateView

#     Variables:
#         fields {tuple} -- [description]
#         form {[type]} -- [description]
#         model {[type]} -- [description]
#         success_url {str} -- [description]
#     """

#     fields = ('username', 'first_name', 'last_name', 'email', 'description', 'user_theme')
#     form = UserCreationForm
#     model = User
#     success_url = '/'
#     success_message = _('Профиль изменен')

#     def get_object(self, queryset=None):
#         return self.request.user


# class createAvatarView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
#     """docstring for DelView"""

#     fields = ('image', 'title')
#     model = Avatar
#     success_message = _('Добавлен новый Аватар')

#     def form_valid(self, form):
#         form.instance.related_obj = self.request.user
#         return super().form_valid(form)

#     def get_success_url(self):
#         return reverse_lazy('acc:profile')


# class deleteAvatarView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
#     """docstring for DelView"""

#     model = Avatar

#     def test_func(self):
#         pk = self.kwargs.get('pk', 0)
#         return self.request.user.pictures.filter(id=pk).exists()

#     def get_success_url(self):
#         return reverse_lazy('acc:profile')


# class changePassword(LoginRequiredMixin, SuccessMessageMixin, dv.PasswordChangeView):
#     template_name = 'registration/change_pass.html'
#     success_message = _('Пароль изменен')

#     def get_success_url(self):
#         return reverse_lazy('acc:profile')


# class passwordResetView(dv.PasswordResetView):
#     template_name = 'registration/password_reset_form.html'
#     email_template_name = 'registration/password_reset_email.html'

#     def post(self, request, *args, **kwargs):
#         return super().post(request, args, kwargs)


# class passwordResetConfirmView(dv.PasswordResetConfirmView):
#     template_name = 'registration/password_reset_confirm.html'
#     success_url = reverse_lazy('acc:password_reset_complete')
#     post_reset_login = True
#     post_reset_login_backend = 'django.contrib.auth.backends.ModelBackend'


# class passwordResetCompleteView(dv.PasswordResetCompleteView):
#     template_name = 'registration/password_reset_complete.html'
