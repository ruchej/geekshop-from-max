from core.models import Core, Picture
from django.contrib.auth.models import AbstractUser, UserManager
from django.utils.translation import gettext_lazy as _
# Create your models here.


class AccountManager(UserManager):
    """
    Object Manager, get manager sets from DB
    """
    def get_managers(self):
        return self.filter(is_staff=True)


class Account(Core, AbstractUser):
    class Meta:
        verbose_name = _("Пользователь")
        verbose_name_plural = _("Пользователи")
    objects = AccountManager()

    """

    username
    first_name
    last_name
    email
    is_staff
    is_active
    date_joined
    password
    last_login

    active
    title
    description
    sort

    pictures

    """

# class Avatar(Picture):
#     """
#     Прокси для Picture`
#     """

#     class Meta:
#         verbose_name = _("Аватар")
#         verbose_name_plural = _("Аватары")
#         proxy = True
