"""geekshop URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

import os
from django.urls import path
from .views import ProductList, ProductDetail, ProductUpdate, AddFormToProductUpdate

app_name = os.path.basename(os.path.dirname(os.path.abspath(__file__)))

urlpatterns = [
    path('<int:pk>/edit/add/', AddFormToProductUpdate.as_view(), name='AddFormToProductUpdate'),
    path('<int:pk>/edit/', ProductUpdate.as_view(), name='ProductUpdate'),
    path('<int:pk>/', ProductDetail.as_view(), name='ProductDetail'),
    # path('category/<int:pk>/', ProductListByCat.as_view(), name='ProductListByCat'),
    path('', ProductList.as_view(), name='all'),
]