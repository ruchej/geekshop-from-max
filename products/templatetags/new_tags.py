from django import template

register = template.Library()


@register.filter
def big_letters(value):
    return str(value).upper()


#@register.simple_tag()