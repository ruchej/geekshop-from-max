from django.shortcuts import render
from django.views.generic import CreateView, DeleteView, DetailView, ListView, RedirectView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.base import View
from django.forms.models import inlineformset_factory
from django.contrib.auth.mixins import  PermissionRequiredMixin
from .models import Product, Category
from .forms import PictureForm, ProductForm, SortingForm, SearchForm, MyFormset
from django.http import JsonResponse, Http404
from django.utils.safestring import mark_safe


class ProductList(ListView):
    """docstring for ProductList"""
    paginate_by = 9
    model = Product
    extra_content = {}
    order_by = ()
    helpers = [SortingForm, SearchForm]

    def get_queryset(self):
        queryset = super().get_queryset()

        for form_class in self.helpers:
            if self.request.method == form_class.method:
                form = form_class(self.request.POST or self.request.GET or None)
                if form.is_valid():
                    queryset = getattr(queryset, form.callback)(**form.cleaned_data)
        return queryset

    def get_context_data(self, **kwargs):
        for form_class in self.helpers:
            kwargs[form_class.__name__] = form_class(self.request.POST or self.request.GET or None)
        return super().get_context_data(**kwargs)


class ProductListByCat(ProductList):
    """docstring for ProductList"""
    slug = 'category__title__icontains'
    pk_field = 'category_id'
    model = Product
    permission_required = f'{model._meta.app_label}.wievew_SPECIAL_CAT_{model.__name__}'

    def get_queryset(self):
        query = Q()
        if 'pk' in self.kwargs:
            query = Q((self.pk_field, self.kwargs['pk']))
        if 'slug' in self.kwargs:
            query = Q((self.slug, self.kwargs['slug']))
        return super().get_queryset().filter(query)


class ProductDetail(DetailView):
    model = Product


class ProductUpdate(PermissionRequiredMixin, UpdateView):
    model = Product
    form_class = ProductForm
    permission_required = f'{model._meta.app_label}.change_{model.__name__}'  # 'products.Product' #
    request = None

    def get_formset_class(self, formset=MyFormset, extra=0, form=PictureForm, fk_name='related_obj', **kwargs):
        return inlineformset_factory(
            self.model, PictureForm._meta.model, formset=formset, extra=extra, form=form, fk_name=fk_name)

    def get_formset(self, **kwargs):
        self.formset = self.get_formset_class(**kwargs)(
            data=self.request and self.request.POST or None,
            files=self.request and self.request.FILES or None,
            instance=getattr(self, 'object', self.get_object()), initial=kwargs.get('initial', {}))
        return self.formset

    def form_valid(self, form, **kwargs):
        response = super().form_valid(form)
        if self.get_formset(**kwargs).is_valid():
            self.formset.save()
        return response

    def get_context_data(self, **kwargs):
        return super().get_context_data(pictures_formset=self.get_formset(), **kwargs)


class AddFormToProductUpdate(PermissionRequiredMixin, View):
    model = Product
    permission_required = f'{model._meta.app_label}.change_{model.__name__}'

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            formset = ProductUpdate(request=request, kwargs=self.kwargs).get_formset()
            if formset.is_valid():
                data = {'can_add_more_forms': len(formset.forms) < formset.max_num}
                if data['can_add_more_forms']:
                    data['form_counter'] = formset.total_form_count() + 1
                    formset = ProductUpdate(kwargs=self.kwargs).get_formset(extra=data['form_counter'])
                    data['name'] = f'{formset.prefix}-inline'
                    data['management'] = formset.render_management()
                    data['form'] = formset.render_form(data['form_counter'], formset[-1])
                return JsonResponse(data)
        return Http404()


class ProductListCat(RedirectView):
    """ 'products/modern/' """
    url = 'products/cat/{yahoo}/'

    def get_redirect_url(self, **kwargs):
        url = self.url.format(self.kwargs)
