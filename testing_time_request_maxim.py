import time
from products.models import Product
from django.db.models.query import ModelIterable, QuerySet


def timer(func):
    def wrapper(*args, **kwargs):
        start = time.time()
        try:
            return func(*args, **kwargs)
        finally:
            print(func.__name__, args[0], time.time() - start)
    return wrapper


def get_request_first(model, *args, **kwargs):
    return model.objects.filter(**kwargs).first()


def get_request_next(model, *args, **kwargs):
    qs = model.objects.filter(**kwargs)
    qs.query.set_limits(high=1)
    return next(qs.iterator(chunk_size=1), None)


def get_request(model, *args, **kwargs):
    qs = model.objects.filter(**kwargs)
    qs.query.set_limits(high=1)
    for prod in qs:
        return prod


def get_request_one(model, *args, **kwargs):
    for prod in model.objects.filter(**kwargs)[:1]:
        return prod


def get_request_get(model, *args, **kwargs):
    try:
        return model.objects.get(**kwargs)
    except:
        pass


def get_like_get(model, *args, **kwargs):
    qs = QuerySet(model).filter(**kwargs).order_by(*args)
    qs.query.set_limits(high=1)
    for obj in qs:
        return obj

    # qquery = Product.objects.filter(title=filter_title)
    # num = len(qquery)
    # if num:
    #    return qquery._result_cache[0]


@timer
def test_func(funk):
    """ТЕСТ запроса с получением первого объекта через FIRST()"""
    title_list = Product.objects.values_list('title', flat=True)
    len_titles = len(title_list)

    obj_counter = 0
    empty_counter = 0
    counter = 0
    while counter < 500:
        for title in title_list:
            for find_title in (title, title[::-1]):
                if funk(Product, title=find_title):
                    obj_counter +=1
                else:
                    empty_counter +=1
                counter += 1
    print(counter, obj_counter, empty_counter)


@timer
def test_func_all_none(funk):
    """ТЕСТ запроса с получением первого объекта через FIRST()"""
    title_list = Product.objects.values_list('title', flat=True)
    len_titles = len(title_list)
    obj_counter = 0
    empty_counter = 0
    counter = 0
    while counter < 500:
        for title in title_list:
            for find_title in (title[::-1], title[::-1]):
                if funk(Product, title=find_title):
                    obj_counter += 1
                else:
                    empty_counter += 1
                counter += 1
    print('all_null', counter, obj_counter, empty_counter)


@timer
def test_func_all_ok(funk):
    """ТЕСТ запроса с получением первого объекта через FIRST()"""
    title_list = Product.objects.values_list('title', flat=True)
    len_titles = len(title_list)
    obj_counter = 0
    empty_counter = 0
    counter = 0
    while counter < 500:
        for title in title_list:
            for find_title in (title, title):
                if funk(Product, title=find_title):
                    obj_counter += 1
                else:
                    empty_counter += 1
                counter += 1
    print('all_ok', counter, obj_counter, empty_counter)


time_dict = {
         'Product.objects.first()': get_request_first,
         'Next in iterator': get_request_next,
         'first in Product.objects.all()': get_request,
         'Product.objects.all()[:1]': get_request_one,
         'like_get': get_like_get,
         'get': get_request_get,
         }


def real_test():
    print('Start')
    for key, val in time_dict.items():
        test_func_all_ok(val)

    for key, val in time_dict.items():
        test_func_all_none(val)

    for key, val in time_dict.items():
        test_func(val)
    print('end')

# if __name__ == '__main__':
#     print('Start')
#     for key, value in time_dict.items():
#         print(key)
#         time_check_dict = test_func(val)
#     print('end')
# Start
# Product.objects.first()
# 2074 1038 1036
# get_request_first 5.404142141342163
# Product.objects.all()[:1]
# 2074 1038 1036
# get_request_one 5.304044485092163
# first in Product.objects.all()
# 2074 1038 1036
# get_request 5.393103837966919
# Next in iterator
# 2074 1038 1036
# get_request_next 5.288544178009033
# end

# >>> real_test()
# Start
# all_ok Next in iterator
# all_ok 2074 2074 0
# get_request_next 5.714822769165039
# all_ok first in Product.objects.all()
# all_ok 2074 2074 0
# get_request 5.416619539260864
# all_ok Product.objects.all()[:1]
# all_ok 2074 2074 0
# get_request_one 5.794872283935547
# all_ok Product.objects.first()
# all_ok 2074 2074 0
# get_request_first 5.634272813796997
# all_none Next in iterator
# all_null 2074 2 2072
# get_request_next 4.99083685874939
# all_none first in Product.objects.all()
# all_null 2074 2 2072
# get_request 5.026357889175415
# all_none Product.objects.all()[:1]
# all_null 2074 2 2072
# get_request_one 5.188967943191528
# all_none Product.objects.first()
# all_null 2074 2 2072
# get_request_first 5.203978776931763
# Next in iterator
# 2074 1038 1036
# get_request_next 5.32056736946106
# first in Product.objects.all()
# 2074 1038 1036
# get_request 5.305554389953613
# Product.objects.all()[:1]
# 2074 1038 1036
# get_request_one 5.529693603515625
# Product.objects.first()
# 2074 1038 1036
# get_request_first 5.834407567977905
# end
# >>>