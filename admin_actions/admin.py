# -*- coding: utf-8 -*-
#coding=utf-8
from django.contrib import admin
#from django.contrib.admin.actions import delete_selected
from django.contrib.auth.models import User, Group, Permission
from django.contrib.sessions.models import Session
from django.contrib.sites.models import Site
from django.utils.translation import get_language, ugettext_lazy as _
from django.contrib.admin.models import LogEntry, ADDITION, CHANGE, DELETION
from django.contrib.contenttypes.models import ContentType
from django.template.response import TemplateResponse

admin.site.unregister(Group)
admin.site.unregister(User)
admin.site.unregister(Site)
#_________________________________________________________________________________-

# 0 пример. Администраторскихе действия в администраторах моделей.
#что это вообще такое, администратор сайта, или администратор модели
#_________________________________________________________________________________-

# 1 пример. отключение функции из глобального списка действий. администратора сайта.
# admin.site.disable_action('delete_selected')

admin.site.disable_action('delete_selected')
#_________________________________________________________________________________-

# 2 пример. Создание новой пустой функции, и включение ее в глобальный список действий для администраторов моделей.
# отключения списка действий для администратора конкретной модели.
def check_active(self, request, queryset):
	pass



# def check_active(self, request, queryset):
# 	pass
# 	admin.site.add_action(check_active, 'check_active')

class PermissionsAdmin(admin.ModelAdmin):
	search_fields = ('name', 'codename','content_type__app_label', 'content_type__model')
	list_display = ('name', 'codename',)
	actions = None

admin.site.register(Permission, PermissionsAdmin)

# actions= None
#_________________________________________________________________________________

# 3 пример. Доработка функции, отключение ее из глобального списка действий и внесение ее в список действий
# администратора модели.
def check_active(self, request, queryset):
	if 'is_active' not in queryset.model._meta.get_all_field_names():
		self.message_user(request, 'нет такого поля')
		return None
	filtered_obj = queryset.filter(is_active=False)
	count = filtered_obj.update(is_active=True)
	self.message_user(request, 'изменено %s' % count)
	return None

check_active.short_description = 'активация объектов'

#admin.site.add_action(check_active, check_active.__name__)

# def check_active(self, request, queryset):
# if 'is_active' in queryset.model._meta.get_all_field_names() :
# 	else:
# 		self.message_user(request, u'нет такого поля is_active' )
#	filtered_obj = queryset.filter(is_active=True)
# 	count = filtered_obj.update(is_active=True)
# 	self.message_user(request, u'%s Пользователей активировано' % count)
# check_active.short_description = u'Выбранные элементы активировать'

# if 'active' in admin.site._actions:
# 	admin.site.disable_action(check_active, check_active.__name__)
# actions= (check_active, )

class SessionAdmin(admin.ModelAdmin):
	fields = ['session_key', 'expire_date', 'session_data', ]
	ordering = ['-expire_date',]
	search_fields = ['session_key', 'session_data', ]
	list_display = ['session_key', 'expire_date', 'session_data', ]
	readonly_fields = ['session_key', 'session_data', 'expire_date']
	actions = (check_active, )

admin.site.register(Session, SessionAdmin)

#_________________________________________________________________________________-

# 4 пример. функция, как метод администратора модели
# def check_passive(self, request, queryset):
# 		filtered_obj = queryset.filter(is_active=True)
# 		count = filtered_obj.update(is_active = False)
# 		self.message_user(request, u'%s деактивировано' % count)
# check_passive.short_description = u'Выбранные элементы деактивировать'
# actions= (check_active, 'check_passive')

class UsersAdmin(admin.ModelAdmin):
	list_display = ('username', 'is_active', 'is_staff' )
	search_fields = ('username',)
	ordering = ('username',)
	actions = ()

	def check_active(self, request, queryset, *args, **kwargs):
		if 'is_active' not in queryset.model._meta.get_all_field_names():
			self.message_user(request, 'нет такого поля')
			return None

		filtered_obj = queryset.filter(is_active=False)



		for obj in filtered_obj:
			app_label = obj._meta.app_label
			obj_model = obj.__class__.__name__.lower()

			ctype_id = ContentType.objects.get(app_label=app_label , model=obj_model ).id

			LogEntry.objects.log_action(
				user_id = request.user.id,
				content_type_id = ctype_id,
				object_id = obj.id,
				object_repr = str(obj),
				action_flag = CHANGE,
				change_message = 'активировали пользователя',
				)

		count = filtered_obj.update(is_active=True)
		self.message_user(request, 'изменено %s' % count)
		return None
	check_active.short_description = 'импорт пользователейо'
	#check_active.allow_permissions = ('add', 'change' ) # add, change, delete, view

	def get_actions(self, request):
		# 1 собирает глобальный список
		# 2. self.actions
		actions = super(UsersAdmin, self).get_actions(request)

		func = check_active
		func_name = func.__name__
		desc = func.short_description

		actions[func_name] = ( func, func_name, desc )
		return actions

	# def get_actions(self, request):
	# 	actions = super(UsersAdmin, self).get_actions(request)
	# 	func = check_active
	# 	name = check_active.__name__
	# 	short_description = getattr(check_active, 'short_description', 'чет я не нашел описания')
	# 	actions[name] = (func, name, short_description)
	# 	return actions

admin.site.register(User, UsersAdmin)

#_________________________________________________________________________________-

# 5 пример. недостатки.

# нет проверки прав
# check_active.allow_permissions = ('edit', 'add')
# check_passive.allow_permissions = ('edit', 'add')

# нет логирования
# action_flag = CHANGE
# log_message = 'деактивировали пользователя'
# user_pk = request.user.id
# app_label=obj.__class__._meta.app_label,
# obj_model=obj.__class__.__name__.lower()
# что такое ContentType - справочник, представляет и хранит информацию о моделях, использующихся в проекте,
# ctype_id = ContentType.objects.get(app_label=app_label, model=obj_model).id
#for obj in filtered_obj:
# LogEntry.objects.log_action(
# 	user_id = user_pk, content_type_id = ctype_id,
# 	object_id = obj.pk, object_repr = obj.__unicode__(),
# 	action_flag = action_flag, change_message= log_message)

# Изначально действие не вызывается если не выбраны пользователи. Решение элегантно. и не сегодня.
#_________________________________________________________________________________

# 6 добавление и проверка функций, в администраторе модели

#if 'delete_selected' not in admin.site._actions:
#	admin.site.add_action(delete_selected, 'delete_selected')

class GrouppenAdmin(admin.ModelAdmin):
	list_display = ('name',)
	search_fields = ('name',)
	ordering = ('name',)

admin.site.register(Group, GrouppenAdmin)

# def get_actions(self, request):
# 	actions = super(self.__class__, self).get_actions(request)
#	app_label = self.model._meta.app_label
# 	obj_model= self.model.__name__.lower()
#actions['pseydo or funk_name'] = (func , 'pseydo or funk_name' , 'normal text to seen (desc)' )
#actions['check_passive'] = (check_passive , check_passive.__name__ , check_passive.short_description if hasattr(check_passive, 'short_description') else 'чет я не нашел описания')
#	user_can_delete = request.user.has_perm('{label}.delete_{model}'.format('label': app_label, 'model': obj_model, ) )
# 	if 'delete_selected' in actions and user_can_delete:
# 		del actions['delete_selected']
# 	return actions

#_________________________________________________________________________________
# 7 Двухшаговая функция с промежуточной страницей

# def check_active(self, request, queryset):
# 	if 'is_active' not in queryset.model._meta.get_all_field_names():
# 		self.message_user(request, u'нет такого поля is_active' )
# 		return None

# 	if request.POST.get('post') == 'yes':
# 		filtered_obj = queryset.filter(is_active=True)
# 		count = filtered_obj.update(is_active=True)
# 		self.message_user(request, u'%s Пользователей активировано' % count)
# 	elif request.POST.get('post') == 'no':
# 			return None

# 	context = {
# 		"title": 'Активация',
# 		"objects_name": self.__class__.__name__,
# 		'queryset': queryset,
# 		'action_objects': [queryset,],
# 		"opts": self.model._meta,
#         "app_label": self.model._meta.app_label,
# 		'action_checkbox_name': admin.helpers.ACTION_CHECKBOX_NAME,
# 		'request' : request,
# 		}
# 	return TemplateResponse(request, ['admin/confirmation.html',], context, current_app=self.admin_site.name)
# check_active.short_description = u'Выбранные элементы активировать'


#_____________________________________________________________________________________________________________
# если останется время.
# if (request.method == 'POST' and 'action' in request.POST and 'index' in request.POST and '_save' not in request.POST):
# 	if not request.POST.getlist(admin.helpers.ACTION_CHECKBOX_NAME):
# 		action = self.get_actions(request).get(request.POST.get('action', 'none'), None)
# 		if getattr(action[0], 'allow_empty',False):
# 			request.POST._mutable = True
# 			request.POST[admin.helpers.ACTION_CHECKBOX_NAME] = ''
# 			request.POST._mutable = False
