function rowhidder(obj){
    while(obj.tagName !== "UL") obj = obj.parentNode;
    obj.classList.add('deleted');
    return false;
}

function filler(button, response){
    button.previousSibling.appendChild('li').outerHTML = response['form'];
    button.previousSibling.children[0].outerHTML = response['management'];
    if (!response['can_add_more_forms']) button.setAttribute('disabled', 'disabled');
}

function addForm(obj){

    var xhttp = new XMLHttpRequest();
    xhttp.responseType = 'json';
    xhttp.timeout = 5000; // time in milliseconds
    xhttp.ontimeout = function (e) {
            // XMLHttpRequest timed out. Do something here.
            console.log('bad timeout');
        };

    xhttp.onreadystatechange = function() {
        if (this.readyState == 4){
            if (this.status && this.status == 200) {
            // handle a successful response
                console.log('good');
                filler(obj, this.response);
            } else {
            // handle a non-successful response
                console.log('bad');
            };
        };
    };

    var data = new FormData(obj.form); // get_data(form.elements, {});
    xhttp.open(obj.form.method, obj.form.action + 'add/', true);
    xhttp.setRequestHeader('X-CSRFToken', data['csrfmiddlewaretoken']);
    xhttp.setRequestHeader('X-Requested-With','XMLHttpRequest');
    xhttp.send(data);
}