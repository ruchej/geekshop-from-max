# -*- coding: utf-8 -*-
import timeit


def string_format(something=None, fmt='Number %s or simply %s'):
    integer = 42
    string = 'FORTY_TWO'
    convert = fmt % (string, integer)


def string_f_string(something=None):
    integer = 42
    string = 'FORTY_TWO'
    convert = f'Number {string} or simply {integer}'


def time(strcommand):
    return f'{timeit.timeit(strcommand, number=int(1e7)):.3f}'


#print(f'Timing  f-string formating:       |  {time("string_f_string()")}')
# print(f'Timing call formating:   |  {time("string_format()")}')
