from django.contrib import admin

# Register your models here.
from .models import Invoice, Order


admin.site.register(Invoice)
