# Generated by Django 2.2.2 on 2019-08-27 10:36

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('core', '0009_auto_20190827_1236'),
        ('products', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Invoice',
            fields=[
                ('core_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='core.Core')),
                ('status', models.CharField(blank=True, default='', max_length=3, verbose_name='Статус')),
                ('date', models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')),
                ('date_modif', models.DateTimeField(auto_now=True, verbose_name='Дата Изменения')),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='invoices', to=settings.AUTH_USER_MODEL)),
            ],
            bases=('core.core',),
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('core_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='core.Core')),
                ('price', models.DecimalField(decimal_places=2, default=0, max_digits=10)),
                ('quantity', models.PositiveIntegerField(default=0)),
                ('date', models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')),
                ('date_modif', models.DateTimeField(auto_now=True, verbose_name='Дата Изменения')),
                ('invioce', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='orders.Invoice')),
                ('ware', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='products.Product')),
            ],
            options={
                'verbose_name': 'Заказ Продукта',
                'verbose_name_plural': 'Заказы Продуктов',
            },
            bases=('core.core',),
        ),
    ]
