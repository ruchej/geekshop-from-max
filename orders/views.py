from django.shortcuts import render
from django.views.generic import CreateView, DeleteView, DetailView, ListView, RedirectView, UpdateView
from .models import Order, Invoice
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.utils.translation import gettext_lazy as _
from products.models import Product
from products.views import ProductDetail
from django.urls import reverse_lazy
from django.core.mail import send_mass_mail, send_mail
from django.contrib.sites.models import Site
from django.http import HttpResponseRedirect
from django.template.loader import render_to_string
from django.contrib.auth.mixins import LoginRequiredMixin
# Create your views here.


class OrderCreate(SuccessMessageMixin, CreateView):
    model = Order
    success_message = _('Товар добавлен')
    fields = ('quantity', 'ware', 'price', 'invoices',)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        update_dict = {'quantity': 0}
        update_dict['ware'] = ProductDetail(kwargs=self.kwargs, pk_url_kwarg='ware_id').get_object()
        update_dict['price'] = update_dict['ware'].price
        update_dict['invoices'] = InvoiceDetail(request=self.request, kwargs=self.kwargs).get_object(create_if_null=True)

        kwargs['initial'] = dict(kwargs.get('initial', {}), **update_dict)
        if 'data' in kwargs:
            kwargs['data'] = dict(kwargs.get('data', {}), **update_dict)
        return kwargs


class OrderAddProduct(SuccessMessageMixin, LoginRequiredMixin, UpdateView):
    model = Order
    fields = ('quantity',)
    success_message = _('Количество товара увеличено')
    success_url = reverse_lazy('orders:InvoiceDetail')

    def get_object(self, queryset=None, create_if_null=False):
        return OrderDetail(request=self.request, kwargs=self.kwargs).get_object(create_if_null=True)

    def form_valid(self, form):
        form.instance.quantity = form.cleaned_data['quantity'] = (form.cleaned_data.get('quantity') or form.instance.quantity) + 1
        respose = super().form_valid(form)
        self.request.session['invoice_summ'] = self.object.invoice.total_summ()
        return respose


class OrderRemoveProduct(SuccessMessageMixin, LoginRequiredMixin, UpdateView):
    model = Order
    success_url = reverse_lazy('orders:InvoiceDetail')
    success_message = _('Количество товара уменьшено')
    fields = ('quantity',)

    def get_object(self, queryset=None, create_if_null=False):
        return OrderDetail(request=self.request, kwargs=self.kwargs).get_object()

    def form_valid(self, form):
        # breakpoint()
        form.instance.quantity = form.cleaned_data['quantity'] = (form.cleaned_data.get('quantity') or form.instance.quantity) - 1
        if form.cleaned_data['quantity'] > 0:
            response = super().form_valid(form)
        else:
            response = OrderDelete(request=self.request, kwargs=self.kwargs).delete(self.request)

        self.request.session['invoice_summ'] = self.object.invoice.total_summ()

        return response

class OrderDetail(DetailView):
    model = Order
    slug_url_kwarg = slug_field = 'ware_id'

    def get_queryset(self):
        return InvoiceDetail(request=self.request).get_object().orders.all()

    def get_object(self, create_if_null=False, **kwargs):
        try:
            return super().get_object()
        except:
            if create_if_null:
                OrderCreate(request=self.request, kwargs=self.kwargs, queryset=self.get_queryset()).post(self.request)
                return self.get_object()


class OrderDelete(SuccessMessageMixin, DeleteView):
    model = Order
    success_url = reverse_lazy('orders:InvoiceDetail')
    success_message = _('товар убран из корзины')
    slug_url_kwarg = slug_field = 'ware_id'

    def delete(self, request, *args, **kwargs):
        super().delete(request, *args, **kwargs)
        return InvoiceDelete(request=self.request, kwargs=self.kwargs).delete(request, *args, **kwargs)


class InvoiceCreate(CreateView):
    model = Invoice
    success_url = reverse_lazy('orders:InvoiceDetail')
    fields = ('owner',)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        update_dict = {'owner': self.request.user} if self.request.user.is_authenticated else {}

        kwargs['initial'] = dict(kwargs.get('initial', {}), **update_dict)
        if 'data' in kwargs:
            kwargs['data'] = dict(kwargs.get('data', {}), **update_dict)
        return kwargs


class InvoiceList(ListView):
    model = Invoice

    def get_queryset(self):
        return super().get_queryset().get_all_from_request(self.request)


class InvoiceDetail(LoginRequiredMixin, DetailView):
    model = Invoice

    def get_object(self, queryset=None, create_if_null=False):
        self.object = self.get_queryset().get_from_request_active(self.request)
        if not self.object and create_if_null:
            InvoiceCreate(request=self.request).post(self.request)
            self.object = self.get_queryset().get_from_request_active(self.request)
        return self.object or self.model()

    def get(self, request, *args, **kwargs):
        if self.get_object().orders.count():
            return super().get(request, *args, **kwargs)

        messages.info(self.request, InvoiceDelete.success_message)
        return HttpResponseRedirect(InvoiceDelete(request=self.request, object=False).get_success_url())


class InvoiceClose(SuccessMessageMixin, LoginRequiredMixin, UpdateView):
    model = Invoice
    fields = ('active',)
    success_url = reverse_lazy('core:index')
    success_message = _('На ваш емайл высланы дальнейшие указания по оплате.')

    def get_object(self, queryset=None):
        return InvoiceDetail(request=self.request).get_object()

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        update_dict = {'active': False}

        kwargs['initial'] = dict(kwargs.get('initial', {}), **update_dict)
        if 'data' in kwargs:
            kwargs['data'] = dict(kwargs.get('data', {}), **update_dict)
        return kwargs

    def form_valid(self, form):
        self.template_name_suffix = f'{self.template_name_suffix}_mail'
        mail_context = self.get_context_data(domain=Site.objects.get_current().domain)
        mail_body = render_to_string(self.get_template_names(), mail_context)
        mail_subject = f'Заказ {self.object.pk} на сайте {Site.objects.get_current().domain}'

        datatuple = [(mail_subject, mail_body, None, [self.object.owner.email, ]), ]  # (subject, message, from_email, recipient_list)

        email_count = send_mass_mail(datatuple, fail_silently=False)

        return super().form_valid(form)


class InvoiceDelete(DeleteView):
    model = Invoice
    not_empty_url = reverse_lazy('orders:InvoiceDetail')
    success_url = reverse_lazy('core:index')
    success_message = _('Ваша корзина пуста')

    def get_object(self, queryset=None):
        return InvoiceDetail(request=self.request).get_object()

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        if 'force' in self.kwargs or not self.object.orders.exists():
            return super().delete(request, *args, **kwargs)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        if self.object and self.object.orders.exists():
            return self.not_empty_url

        messages.info(self.request, self.success_message)
        return self.success_url
